package sloop.fileWorkers

import spock.lang.Specification

/**
 * Created by robertarnold on 8/19/16.
 */
class SystemKtTest extends Specification {
    void setup() {

    }

    void cleanup() {
        new File("conf").deleteDir()
        new File("data").deleteDir()
        new File("out").deleteDir()
        new File("pipeline").deleteDir()
        new File("scripts").deleteDir()
    }

    def "BootstrapDirectories"() {
        when:
            sloop.fileWorkers.SystemKt.bootstrapDirectories()

        then:
            new File("conf").isDirectory()
            new File("data").isDirectory()
            new File("out").isDirectory()
            new File("pipeline").isDirectory()
            new File("scripts").isDirectory()
    }

    def "BootstrapDirectoriesWLanguage"() {

    }

    def "CleanOutputDirectory"() {
        when:
            new File("out").mkdir()
            new File("out/test.txt").createNewFile()
            sloop.fileWorkers.SystemKt.cleanOutputDirectory()

        then:
            new File("out").listFiles().length == 0
    }
}
