package sloop

import org.apache.commons.cli.Option
import org.apache.commons.cli.Options
import spock.lang.Specification

/**
 * Created by robertarnold on 8/19/16.
 */
class MainKtSpec extends Specification {
    Options options = MainKt.returnOptions()
    def initOptions = "-i"
    def cleanOptions = "-cl"

    void setup() {

    }

    void cleanup() {
        new File("conf").deleteDir()
        new File("data").deleteDir()
        new File("out").deleteDir()
        new File("pipeline").deleteDir()
        new File("scripts").deleteDir()
    }

    def "Main"() {
        testInit()
        testClean()
    }

    def testInit() {
        /*
            Test 1 - validate the init option creates all necessary directories
        */
        when:
            sloop.MainKt.main(initOptions.split())

        then:
            new File("conf").isDirectory()
            new File("data").isDirectory()
            new File("out").isDirectory()
            new File("pipeline").isDirectory()
            new File("scripts").isDirectory()
    }

    def testClean() {
        /*
            Test 2 - validate the clean option removes the content of out
        */
        when:
            sloop.MainKt.main(initOptions.split())
            new File("out/test.txt").createNewFile()
            sloop.MainKt.main(cleanOptions.split())

        then:
            new File("out").isDirectory()
            new File("out").listFiles().length == 0
    }


    def "ReturnOptions"() {
        expect:
            options.getOption("i") == new Option("i", "init", false, "")
            options.getOption("l") == new Option("l", "language", false, "")
            options.getOption("co") == new Option("co", "config", false, "")
            options.getOption("cl") == new Option("cl", "clean", false, "")
    }
}
