package sloop

import org.apache.commons.cli.DefaultParser
import org.apache.commons.cli.Options
import sloop.fileWorkers.cleanOutputDirectory

/**
 * Created by robertarnold on 8/16/16.
 */
fun main(args: Array<String>) {
    val parser = DefaultParser()
    val cmd = parser.parse(returnOptions(), args)

    //Init workflow and logic
    if (cmd.hasOption("init")) {
        if (cmd.hasOption("language")) {
            sloop.fileWorkers.bootstrapDirectoriesWLanguage(cmd.getOptionValue("language"))
        } else {
            sloop.fileWorkers.bootstrapDirectories()
        }
    }

    //Clean workflow and logic
    if (cmd.hasOption("clean")) {
        cleanOutputDirectory(null)
    }
}

fun returnOptions(): Options {
    val options = Options()
    options.addOption( "i", "init", false, "Initializes a new project directory" )
    options.addOption( "l", "language", true, "Initializes a new project directory" )
    options.addOption( "cl", "clean", false, "Cleans the output directory" )
    options.addOption( "co", "config", true, "Specifies the configuration file to use")

    return options
}
