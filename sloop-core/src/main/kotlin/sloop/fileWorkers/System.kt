package sloop.fileWorkers

/**
 * Created by robertarnold on 8/17/16.
 */
import java.io.File

fun bootstrapDirectories() {
    System.out.println("bootstrapping")
    File("data").mkdir()
    File("scripts").mkdir()
    File("pipeline").mkdir()
    File("conf").mkdir()
    File("out").mkdir()
    File("README.md").createNewFile()
    File("results.md").createNewFile()
}


fun bootstrapDirectoriesWLanguage(language: String) {
    bootstrapDirectories()
    /*
        Add secondary language option
     */
}

fun cleanOutputDirectory(file: File?) {
    for (file in File("out").listFiles()) {
        if (file.isDirectory()) cleanOutputDirectory(file)
        file.delete()
    }
}