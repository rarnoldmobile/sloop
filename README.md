# sloop

## Data Science Environment Toolkit

### Project Goals:

*   To create a repeatable process for bootstrapping data science projects
*   To create a flexible framework for establishing a data pipeline

### Examples:

Generating a new environment:
```
sloop init
```


Cleaning your output folder:
```
sloop clean
```


Run the default pipeline:
```
sloop pipeline
```


Run a specific pipeline:
```
sloop pipeline hello
```

### Configuration File Format:
A project can start off with a generic configuration file.
If detected, when you perform a sloop init - the configuration file will determine various options used.

Currently, the support configuration file format is written in yaml.  The following options are available:

languages: [SAS, R, Python]

A sample configuration file is found underneath the "sample" directory.

The filename that sloop will look for is "sloop-config.yaml"  You can override this with the co/config flag:

```
sloop init -config=another-config.yaml
```


### Rationale:
What should a pipeline contains?

*   data
    * all data files should be saved here
*   pipeline
    * definition of each workflow that is being generated
*   scripts
    * a collection of all scripts that are being used in the pipeline
    * each technology type will have it's own subdirectory
        * python
        * r
        * sas
*   conf
    * a configuration directory specifying the project pipline
*   root directory should contain
    * README.md
    * results.md


### FAQ:

*   Isn't this a little excessive?  Doesn't this remove creativity associated with data science?  Or do you believe this is just a rationale way to organize thoughts?
    > Yes!

*   Wait, what did that answer mean?
    > Yes!

*   Fine, screw it - what are the system dependencies?
    > Java

*   Can I change it?
    > Yes!

*   Will you accept pull requests?
    > No!
